package com.example.tailorhelper

import androidx.lifecycle.MutableLiveData

class Parameters {

    var sg3 = MutableLiveData<String?>(null)
    var pg = MutableLiveData<String?>(null)
    var pof = MutableLiveData<String?>(null)
    var shs = MutableLiveData<String?>(null)
    var pshs = MutableLiveData<String?>(null)
    var shg = MutableLiveData<String?>(null)
    var sg2 = MutableLiveData<String?>(null)
    var sg1 = MutableLiveData<String?>(null)
    var pshp = MutableLiveData<String?>(null)
    var op = MutableLiveData<String?>(null)
    val dts2 = MutableLiveData<String?>(null)
    var vprz2 = MutableLiveData<String?>(null)
    var pspr = MutableLiveData<String?>(null)
    var pdts = MutableLiveData<String?>(null)
    var di = MutableLiveData<String?>(null)

    fun checkingParametersForNull(): Boolean {
        return sg3.value == null ||
                pg.value == null ||
                pof.value == null ||
                shs.value == null ||
                pshs.value == null ||
                shg.value == null ||
                sg2.value == null ||
                sg1.value == null ||
                pshp.value == null ||
                op.value == null ||
                dts2.value == null ||
                vprz2.value == null ||
                pspr.value == null ||
                pdts.value == null ||
                di.value == null
    }

}