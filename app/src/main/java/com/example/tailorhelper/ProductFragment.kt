package com.example.tailorhelper

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.tailorhelper.databinding.FragmentProductBinding

class ProductFragment : Fragment() {

    private val productViewModel: ProductViewModel by lazy {
        ViewModelProvider(this).get(ProductViewModel::class.java)
    }
    private lateinit var viewBinding: FragmentProductBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentProductBinding.inflate(inflater, container, false)
        viewBinding.parameters = productViewModel.parameters
        viewBinding.productViewModel = productViewModel
        viewBinding.productFragment = this
        viewBinding.lifecycleOwner = this

        return viewBinding.root
    }

    fun calculate() {
        if (productViewModel.parameters.checkingParametersForNull()) {
            Toast.makeText(context, R.string.some_parameters_is_null_toast, Toast.LENGTH_SHORT)
                .show()
        } else {
            productViewModel.calculateAllResults()
        }
    }
}