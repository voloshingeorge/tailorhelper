package com.example.tailorhelper

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val currentFragment =
            supportFragmentManager.findFragmentById(R.id.portrait_product_fragment_container)

        if (currentFragment == null) {
            supportFragmentManager.beginTransaction().apply {
                add(R.id.portrait_product_fragment_container, ProductFragment())
                commit()
            }
        }

    }

}