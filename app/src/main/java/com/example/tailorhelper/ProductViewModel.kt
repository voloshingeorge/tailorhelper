package com.example.tailorhelper

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.math.pow
import kotlin.math.roundToInt

class ProductViewModel : ViewModel() {

    val parameters = Parameters()

    var result1 = MutableLiveData<String?>(null)
    var result2 = MutableLiveData<String?>(null)
    var result3 = MutableLiveData<String?>(null)
    var result4 = MutableLiveData<String?>(null)
    var result5 = MutableLiveData<String?>(null)
    var result6 = MutableLiveData<String?>(null)
    var result7 = MutableLiveData<String?>(null)
    var result8 = MutableLiveData<String?>(null)
    var result9 = MutableLiveData<String?>(null)

    private fun calculateResult1() {
        result1.value =
            pow(parameters.sg3.value!!.toDouble() + parameters.pg.value!!.toDouble() + parameters.pof.value!!.toDouble())
                .toString()
    }

    private fun calculateResult2() {
        result2.value =
            pow(parameters.shs.value!!.toDouble() + parameters.pshs.value!!.toDouble())
                .toString()
    }

    private fun calculateResult3() {
        result3.value =
            pow(
                parameters.shg.value!!.toString()
                    .toDouble() + (parameters.sg2.value!!.toDouble() - parameters.sg1.value!!.toDouble()) + parameters.pshp.value!!.toDouble()
            ).toString()
    }

    private fun calculateResult4() {
        result4.value =
            pow(0.36 + parameters.op.value!!.toDouble() + 1.5)
                .toString()
    }

    private fun calculateResult5() {
        result5.value =
            pow(0.4 * parameters.dts2.value!!.toDouble() - 2)
                .toString()
    }

    private fun calculateResult6() {
        result6.value =
            pow(parameters.vprz2.value!!.toDouble() + parameters.pspr.value!!.toDouble() + 0.5 * parameters.pdts.value!!.toDouble())
                .toString()
    }

    private fun calculateResult7() {
        result7.value =
            pow(parameters.dts2.value!!.toDouble() + parameters.pdts.value!!.toDouble())
                .toString()
    }

    private fun calculateResult8() {
        result8.value =
            pow(0.5 * parameters.dts2.value!!.toDouble() - 2)
                .toString()
    }

    private fun calculateResult9() {
        result9.value =
            pow(parameters.di.value!!.toDouble() + parameters.pdts.value!!.toDouble())
                .toString()
    }

    fun calculateAllResults() {
        calculateResult1()
        calculateResult2()
        calculateResult3()
        calculateResult4()
        calculateResult5()
        calculateResult6()
        calculateResult7()
        calculateResult8()
        calculateResult9()
    }

    private fun pow(number: Double, scale: Int = 1): Double {
        val pow = 10.0.pow(scale)
        return (number * pow).roundToInt() / pow
    }

}